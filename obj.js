function Background() {
    bg = new Sprite(scene, "island.gif", 960, 539);
    bg.setSpeed(0, 0);
    bg.setPosition(480, 269.5);

    return bg;
}

function Snail() {
    var paddingX = 60;
    var paddingY = 80;
    spriteSnail = new Sprite(scene, "snailfemale.png", 144, 192);
    spriteSnail.loadAnimation(144, 192, 48, 48);
    spriteSnail.generateAnimationCycles();
    spriteSnail.renameCycles(new Array("down", "left", "right", "up"));
    spriteSnail.setAnimationSpeed(1000);
    spriteSnail.collidesWithRect = function (otherSprite) {  // Calculate the boundaries of the snail
        paddingX;
        paddingY;
        var myLeft = this.x - (this.width / 2) + paddingX;
        var myRight = this.x + (this.width / 2) - paddingX;
        var myTop = this.y - (this.height / 2) + paddingY;
        var myBottom = this.y + (this.height / 2) - paddingY;

        // Calculate the boundaries of the other sprite
        var otherLeft = otherSprite.x - otherSprite.width / 2;
        var otherRight = otherSprite.x + otherSprite.width / 2;
        var otherTop = otherSprite.y - otherSprite.height / 2;
        var otherBottom = otherSprite.y + otherSprite.height / 2;

        // Check if the boundaries overlap
        return !(myLeft > otherRight || myRight < otherLeft || myTop > otherBottom || myBottom < otherTop);
    };

    //start and paused
    spriteSnail.setPosition(440, 380);
    spriteSnail.setSpeed(0);
    spriteSnail.pauseAnimation();
    spriteSnail.setCurrentCycle("down");
    spriteSnail.setBoundAction(STOP);

    spriteSnail.moveControl = function () {
        if (keysDown[K_LEFT] || keysDown[K_A]) {
            this.setSpeed(5);
            this.playAnimation()
            this.setMoveAngle(270);
            this.setCurrentCycle("left");
        }
        if (keysDown[K_RIGHT] || keysDown[K_D]) {
            this.setSpeed(5);
            this.playAnimation()
            this.setMoveAngle(90);
            this.setCurrentCycle("right");
        }
        if (keysDown[K_UP] || keysDown[K_W]) {
            this.setSpeed(5);
            this.playAnimation()
            this.setMoveAngle(0);
            this.setCurrentCycle("up");
        }
        if (keysDown[K_DOWN] || keysDown[K_S]) {
            this.setSpeed(5);
            this.playAnimation()
            this.setMoveAngle(180);
            this.setCurrentCycle("down");
        }

        if (keysDown[K_SPACE]) {
            this.setSpeed(0);
            this.pauseAnimation();
            this.setCurrentCycle("down");
        }
    }

    return spriteSnail;
}

function Apple() {
    spriteApple = new Sprite(scene, "apple1.png", 32, 37);

    // spriteApple = new Sprite(scene, "Apple.png", 850, 50);
    // spriteApple.loadAnimation(850, 50, 50, 50);
    // spriteApple.generateAnimationCycles();

    // spriteApple.setPosition(400, 380);
    
    spriteApple.collidesWithRect = function (otherSprite) {
        // Calculate the boundaries of the snail
        var myLeft = this.x - this.width / 2;
        var myRight = this.x + this.width / 2;
        var myTop = this.y - this.height / 2;
        var myBottom = this.y + this.height / 2;

        // Calculate the boundaries of the other sprite
        var otherLeft = otherSprite.x - otherSprite.width / 2;
        var otherRight = otherSprite.x + otherSprite.width / 2;
        var otherTop = otherSprite.y - otherSprite.height / 2;
        var otherBottom = otherSprite.y + otherSprite.height / 2;

        // Check if the boundaries overlap
        return !(myLeft > otherRight || myRight < otherLeft || myTop > otherBottom || myBottom < otherTop);
    };
    spriteApple.setSpeed(0);

    spriteApple.motionApple = function () {
        newDir = (Math.random() * 90) - 45;
        this.changeAngleBy(newDir);
    }

    spriteApple.reset = function () {
        // Define the allowed areas
        var allowAreas = [
            { x1: 100, y1: 100, x2: 850, y2: 400 },
            // Add more allowed areas here
        ];
    
        var spawnedInAllowedArea = false;
    
        while (!spawnedInAllowedArea) {
            newX = Math.random() * this.cWidth;
            newY = Math.random() * this.cHeight;
    
            for (var i = 0; i < allowAreas.length; i++) {
                if (newX > allowAreas[i].x1 && newX < allowAreas[i].x2 && newY > allowAreas[i].y1 && newY < allowAreas[i].y2) {
                    spawnedInAllowedArea = true;
                    break;
                }
            }
        }
    
        this.setPosition(newX, newY);
    } // end reset

    return spriteApple;
}

function Bomb() {
    spriteBomb = new Sprite(scene, "bomb.png", 32, 32);
    spriteBomb.collidesWithRect = function (otherSprite) {
        // Calculate the boundaries of the snail
        var myLeft = this.x - this.width / 2;
        var myRight = this.x + this.width / 2;
        var myTop = this.y - this.height / 2;
        var myBottom = this.y + this.height / 2;

        // Calculate the boundaries of the other sprite
        var otherLeft = otherSprite.x - otherSprite.width / 2;
        var otherRight = otherSprite.x + otherSprite.width / 2;
        var otherTop = otherSprite.y - otherSprite.height / 2;
        var otherBottom = otherSprite.y + otherSprite.height / 2;

        // Check if the boundaries overlap
        return !(myLeft > otherRight || myRight < otherLeft || myTop > otherBottom || myBottom < otherTop);
    };
    spriteBomb.setPosition(600, 330);
    spriteBomb.setSpeed(0);

    spriteBomb.reset = function (apples) {
        // Define the allowed areas
        var allowAreas = [
            { x1: 100, y1: 100, x2: 850, y2: 400 },
            // Add more allowed areas here
        ];
    
        var spawnedInAllowedArea = false;
        var collidesWithApple = true;

    while (!spawnedInAllowedArea || collidesWithApple) {
            newX = Math.random() * this.cWidth;
            newY = Math.random() * this.cHeight;
    
            for (var i = 0; i < allowAreas.length; i++) {
                if (newX > allowAreas[i].x1 && newX < allowAreas[i].x2 && newY > allowAreas[i].y1 && newY < allowAreas[i].y2) {
                    spawnedInAllowedArea = true;
                    break;
                }
            }
            // Check for collisions with apples
        if (spawnedInAllowedArea) {
            collidesWithApple = checkBombAppleCollision(this, apples);
        }
        }
    
        this.setPosition(newX, newY);
    } // end reset

    return spriteBomb;
}